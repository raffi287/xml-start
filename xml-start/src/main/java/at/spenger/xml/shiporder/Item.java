//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 

package at.spenger.xml.shiporder;

import javax.xml.bind.annotation.XmlAttribute;

public class Item {

    @XmlAttribute(required = true)
    protected String currency;
    @XmlAttribute(required = true)
    protected double rate;

    @Override
	public String toString() {
		return "Item [currency=" + currency + ", rate=" + rate + "]";
	}

	public void setCurrency(String currency){
		this.currency=currency;
	}
	
	public String getCurrency(){
		return this.currency;
	}
	
	public void setRate (double rate){
		this.rate=rate;
	}
	
	public double getRate(){
		return this.rate;
	}
	

}

